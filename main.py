from color_data import color_list
from turtle import colormode, Screen, Turtle
import random as r

t = Turtle()
t.speed("fastest")
t.penup()
t.hideturtle()
colormode(255)
s = Screen()

t.seth(255)
t.forward(300)
t.seth(0)
number_of_dots = 100

for dot_count in range(1, number_of_dots + 1):
    t.dot(20, r.choice(color_list))
    t.forward(50)

    if dot_count % 10 == 0:
        t.seth(90)
        t.forward(50)
        t.seth(180)
        t.forward(500)
        t.seth(0)

s.exitonclick()

